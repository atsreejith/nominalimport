﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelImport.Models
{
    [Table("VAT_XERO_Nominal")]
    public class Nominal
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid ID { get; set; }
        public int OrgID { get; set; }
        public DateTime Date { get; set; }
        public string Source { get; set; }
        public string Description { get; set; }
        public string Reference { get; set; }
        public float Debit { get; set; }
        public float Credit { get; set; }
        public float Amount { get; set; }
        public string ComparisonValue { get; set; }
        
    }
}
