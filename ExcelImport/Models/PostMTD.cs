﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelImport.Models
{
    [Table("VAT_XERO_PostMTD")]
    public class PostMTD
    {
        public Guid ID { get; set; }
        public int OrgID { get; set; }
        public DateTime VAT_Period_End { get; set; }
        public float VAT_Rate { get; set; }
        public DateTime Date { get; set; }

        [StringLength(30)]
        public string Account { get; set; }
        [StringLength(30)]
        public string Reference { get; set; }
        [StringLength(200)]
        public string Details { get; set; }
        [StringLength(150)]
        public string Details1 { get; set; }
        [StringLength(150)]
        public string Details2 { get; set; }
        public float VAT { get; set; }
        public float Net { get; set; }
        [StringLength(200)]
        public string ComparisonValue { get; set; }

    }
}
