﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelImport.Models.Enum
{
    public enum ImportType
    {
        Nominal,
        PreMTD,
        PostMTD
    }
}
