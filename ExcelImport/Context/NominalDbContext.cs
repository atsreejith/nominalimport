﻿using ExcelImport.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelImport.Context
{
    public class NominalDbContext : DbContext
    {
        public NominalDbContext() : base("NominalConnection")
        {
        }


        public DbSet<Nominal> Nominals { get; set; }

        public DbSet<PreMTD> PreMTDs { get; set; }

        public DbSet<PostMTD> PostMTDs { get; set; }

    }
}
