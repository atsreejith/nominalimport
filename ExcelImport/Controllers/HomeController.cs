﻿using ExcelDataReader;
using ExcelImport.Context;
using ExcelImport.Models;
using ExcelImport.Models.Enum;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcelImport.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Import(HttpPostedFileBase uploadFile,ImportType importType)
        {
            switch (importType)
            {
                case ImportType.Nominal:
                    ImportNominal(uploadFile);
                    break;
                case ImportType.PreMTD:
                    ImportPreMTD(uploadFile);
                    break;
                case ImportType.PostMTD:
                    ImportPostMTD(uploadFile);
                    break;
                default:
                    break;
            }

            return RedirectToAction("Index");
        }


        private void ImportNominal(HttpPostedFileBase uploadFile)
        {
            var importData = new List<Nominal>();

            using (var stream = uploadFile.InputStream)
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var ValidDate = new DateTime();
                            if (DateTime.TryParse(reader.GetValue(0)?.ToString(), out ValidDate))
                            {
                                var line = new Nominal
                                {
                                    ID = Guid.NewGuid(),
                                    OrgID = 0,
                                    Date = reader.GetDateTime(0),
                                    Source = reader.GetString(1) ?? string.Empty,
                                    Description = reader.GetString(2) ?? string.Empty,
                                    Reference = reader.GetString(3) ?? string.Empty,
                                    Debit = float.Parse(reader.GetValue(4)?.ToString()),
                                    Credit = float.Parse(reader.GetValue(5)?.ToString()),
                                };
                                line.Amount = line.Debit - line.Credit;
                                line.ComparisonValue = $"{line.Date.ToShortDateString()}/{line.Description}/{line.Amount}";
                                importData.Add(line);
                            }
                        }
                    } while (reader.NextResult());
                }
            }
            using (var context = new NominalDbContext())
            {
                context.Nominals.AddRange(importData);
                var result = context.SaveChanges();
            }
        }

        private void ImportPreMTD(HttpPostedFileBase uploadFile)
        {
            try
            {
                var importData = new List<PreMTD>();


                using (var stream = uploadFile.InputStream)
                {
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        var data = reader.AsDataSet().Tables[1];
                        foreach (var row in data.Rows)
                        {
                            var ValidDate = new DateTime();
                            var rowData = ((DataRow)row);
                            if (DateTime.TryParse(rowData[0]?.ToString(), out ValidDate))
                            {
                                var line = new PreMTD
                                {
                                    ID = Guid.NewGuid(),
                                    OrgID = 0,
                                    VAT_Period_End = DateTime.Now,// expecting a user input here
                                    Date = Convert.ToDateTime(rowData[0].ToString()),
                                    VAT_Rate = 0,
                                    Account = rowData[1].ToString() ?? string.Empty,
                                    Reference = rowData[2].ToString() ?? string.Empty,
                                    Details = rowData[3].ToString() ?? string.Empty,
                                    Gross = float.Parse(rowData[4].ToString()),
                                    VAT = float.Parse(rowData[5].ToString()),
                                    Net = float.Parse(rowData[6].ToString())

                                };
                                line.Details1 = line.Details.Contains(":") ? line.Details.Split(':')[1].Trim() : line.Details.Contains("-") ? line.Details.Split('-')[0].Trim() : string.Empty;
                                line.Details2 = line.Details.Contains(":") ? line.Details.Split(':')[0].Trim() : line.Details.Contains("-") ? line.Details.Split('-')[1].Trim() : string.Empty;
                                line.VAT_Rate = (line.VAT / line.Net) * 100;
                                line.ComparisonValue = $"{line.Date.ToShortDateString()}/{line.Details1}/{line.VAT}";
                                importData.Add(line);
                            }
                        }
                    }
                }
                using (var context = new NominalDbContext())
                {
                    context.PreMTDs.AddRange(importData);
                    var result = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void ImportPostMTD(HttpPostedFileBase uploadFile)
        {
            try
            {
                var importData = new List<PostMTD>();

                using (var stream = uploadFile.InputStream)
                {
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        var data = reader.AsDataSet().Tables[1];
                        foreach (var row in data.Rows)
                        {
                            var ValidDate = new DateTime();
                            var rowData = ((DataRow)row);
                            if (rowData[0].ToString() == "Box 6")
                                break;

                            if (DateTime.TryParse(rowData[0]?.ToString(), out ValidDate))
                            {
                                var line = new PostMTD
                                {
                                    ID = Guid.NewGuid(),
                                    OrgID = 0,
                                    VAT_Period_End = DateTime.Now,// expecting a user input here
                                    Date = Convert.ToDateTime(rowData[0].ToString()),
                                    VAT_Rate = 0,
                                    Account = rowData[1].ToString() ?? string.Empty,
                                    Reference = rowData[2].ToString() ?? string.Empty,
                                    Details = rowData[3].ToString() ?? string.Empty,
                                    VAT = float.Parse(rowData[4].ToString()),
                                    Net = float.Parse(rowData[5].ToString())

                                };
                                line.Details1 = line.Details.Contains(":") ? line.Details.Split(':')[1].Trim() : line.Details.Contains("-") ? line.Details.Split('-')[0].Trim() : string.Empty;
                                line.Details2 = line.Details.Contains(":") ? line.Details.Split(':')[0].Trim() : line.Details.Contains("-") ? line.Details.Split('-')[1].Trim() : string.Empty;
                                line.VAT_Rate = (line.VAT / line.Net) * 100;
                                line.ComparisonValue = $"{line.Date.ToShortDateString()}/{line.Details1}/{line.VAT}";
                                importData.Add(line);
                            }
                        }
                    }
                }
                using (var context = new NominalDbContext())
                {
                    context.PostMTDs.AddRange(importData);
                    var result = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}